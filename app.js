var express = require('express');
var bodyParser = require('body-parser');
//var usersFile = require('./users.json');
//var accountsFile = require('./accounts.json');
var requestJson = require('request-json');
var constantes = require('./constantes.js');

var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var urlMlabRaiz = constantes.urlMlabRaiz;
var apiKey = constantes.apiKey;
var query = constantes.query;
const URI = constantes.URI;

app.listen(port);
console.log('Escuchando en el puerto 3000');


var clienteMlab;

// Obtener Usuarios
app.get(URI +"Usuarios", function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
          res.send(body);
        };
    });
});

// Obtener usuario por id
app.get (URI +'UsuariosId/:id',  function (req,res) {
    var id = req.params.id;
    var query = 'q={"id":' + id + '}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + query + "&l=1&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
            if (body.length > 0){
              res.send(body[0]);
            } else {
              res.status(404).send('Usuario no encontrado');
            }
        }
        else {
          res.send(body);
        }
      });
});

// Obtener usuario por mail
app.get (URI +'UsuariosMail/:email',  function (req,res) {
       var email = req.params.email;
       console.log(email);
       var query = 'q={"email":' + '"' + email + '"' + '}';
       clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + query + "&l=1&" + apiKey);
        clienteMlab.get('', function(err, resM, body) {
            if (!err) {
              console.log(body);

              if (body.length > 0){
                res.send(body[0]);
              }else {
                res.status(404).send('Usuario no encontrado');
              }

            }
            else {
              res.send(body);
            }
          });
});

// Crear usuario
app.post(URI + 'usersmlabcrear',
  function(req, res){
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + query + apiKey);
    clienteMlab.get('', function(error, resM, body){
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        console.log(newUser);
        clienteMlab.post(urlMlabRaiz + "/user?" + apiKey, newUser,
          function(error, resM, body){
            res.send(body);
          });
      });
  });

  // Actualizar usuario
  app.put(URI + 'usersmlab/:id', function(req, res){
  console.log('put users');
  let id = req.params.id;

  var queryFiltro = 'q={"id":' + id + '}';

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + queryFiltro + "&l=1&" + apiKey);

  clienteMlab.get('', function(error, resM, body){

    if (!error) {
      if (body.length > 0){
        var usuarioUpdate=req.body; //los datos a actualizar, cuerpo de la peticion
        usuarioUpdate.id= body[0].id;//el body que recibo (la respuesta) viene como array
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/user");

        clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey,usuarioUpdate ,
           function(errP, resP, bodyP) {
             if (!errP) {
               res.send("Usuario actualizado correctamente");
             }else{
               res.status(500).send('Error al actualizar usuario');
             }
          })
      }else {
        res.status(404).send('Usuario no encontrado');
      }
    }else{
      res.status(500).send('Error obteniendo usuario');
    }
  });
  });

  /*
  var response = {};
  if (body.n >0){ //.n cantidad de registros del body
  response = {
    "msg" :  "usuario actualizado";
  }
}

$[userid]// se usa para concatenar el valor que tenga userid
  */

  // Eliminar usuario
  app.delete(URI + 'usersmlab/:id', function(req, res){
   console.log('delete users');
   let id = req.params.id;

   var queryFiltro = 'q={"id":' + id + '}';
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryFiltro + "&l=1&" + apiKey);

   clienteMlab.get('', function(error, resM, body){

     if (!error) {
       if (body.length > 0){
         var idUsuarioMongo=body[0]._id.$oid;
         clienteMlab = requestJson.createClient(urlMlabRaiz + "/user/");

         clienteMlab.delete(idUsuarioMongo + '?' + apiKey ,
            function(errP, resP, bodyP) {
              if (!errP) {
                res.send("Usuario eliminado correctamente");
              }else{
                res.status(500).send('Error al eliminar usuario');

              }

           })

       }else {
         res.status(404).send('Usuario no encontrado');
       }
     }else{
       res.status(500).send('Error obteniendo usuario');
     }
   });

  });


// Login usuario
app.post(URI + "login",
  function (req, res){
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryStringEmail + "&l=1&" + apiKey);
    clienteMlab.get('' ,function(error, respuestaM , body) {
      var respuesta = body[0];
      if(respuesta != undefined){
          if (respuesta.password == pass) {
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(login),
              function(errorP, respuestaM, bodyP) {
                res.send(body[0]);
              });
          }
          else {
            res.send({"msg":"contraseña incorrecta"});
          }
      } else {
        res.send({"msg": "email Incorrecto"});
      }
    });
});


// Logout usuario
app.post(URI + "logout", function (req, res){

   var email = req.body.email;
   var queryStringEmail = 'q={"email":"' + email + '"}&';

   clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryStringEmail + "&l=1&" + apiKey);
   clienteMlab.get('' ,function(error, respuestaMLab, body) {

     var respuesta = body[0];
     console.log(respuesta);

     if(respuesta != undefined){

           var session = {"logged":true};
           var logout = '{"$unset":' + JSON.stringify(session) + '}';

           clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {
               if(!errorP){
                 res.send(body[0]);
               }else{
                 res.status(500).send('Ocurrió un error en el login');
               };
             });
     } else {
       res.status(500).send('Ocurrió un error al realizar el logout');
     }
   });
 });


 // Crear nuevo usuario
 app.post(URI + 'Usuario/:email', function(req, res){
   let email = req.params.email;

   var queryFiltro = 'q={"email":' + '"' + email + '"' + '}';
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + queryFiltro + "&l=1&" + apiKey);

   clienteMlab.get('', function(error, resM, body){

     if (!error) {
       if (body.length == 0){
         //No se encontrò el usuario, se puede ingresar
         //Se obtiene el ultimo id creado
         clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + apiKey);
         clienteMlab.get('', function(errorId, resMId, bodyId){
           if(!errorId){

             newID = bodyId.length + 1;

             var newUser = {
               "id" : newID,
               "first_name" : req.body.first_name,
               "last_name" : req.body.last_name,
               "email" : req.body.email,
               "password" : req.body.password
             };

             console.log(newUser);
             clienteMlab.post(urlMlabRaiz + "/user?" + apiKey, newUser,
               function(error, resM, body){
                 res.send(body);
               });

           }
           else{
             res.send('Ha ocurrido un error al obtener los usuarios');
           }

       });

     }else {
       res.send('Usuario ya existe');
     }

   }else{
     res.status(500).send('Error obteniendo usuario');
   }

   });
 });


//Modificar contraseña

app.put(URI + 'UsuarioPassword/:id', function(req, res){
    let id = req.params.id;
    var queryFiltro = 'q={"id":' + id + '}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + queryFiltro + "&l=1&" + apiKey);
    clienteMlab.get('', function(error, resM, body){

      if (!error) {
        if (body.length > 0){
          //var usuarioUpdate = req.body; //los datos a actualizar, cuerpo de la peticion
          console.log(body[0]);

          var passActual = body[0].password
          var usuarioUp = body[0];
          usuarioUp.password = req.body.password;

          if(body[0].logged == true)
          {
              if(usuarioUp.password != passActual)
              {
                clienteMlab = requestJson.createClient(urlMlabRaiz + "/user");

                clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, usuarioUp ,
                   function(errP, resP, bodyP) {
                     if (!errP) {
                       res.send("Usuario actualizado correctamente");
                     }else{
                       res.status(500).send('Error al actualizar usuario');
                     }
                  })

                }
                else{
                  res.send('La contraseña coincide con la contraseña anterior');
                }
            }
            else{
              res.status(200).send('Algunos de los datos no son correctos');
            }
        }
        else {
          res.status(404).send('Usuario no encontrado');
        }
      }
      else{
        res.status(500).send('Error obteniendo usuario');
      }
    });
});


// Obtener cuentaporIdUsuario
app.get(URI + 'CuentaUsuario/:id', function(req, res) {
  var idUsuario = req.params.id;
  var query = 'q={"idUsuario":' + idUsuario + '}';

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + query + "&l=1&" + apiKey);

  //console.log(urlMlabRaiz + "/Cuentas?f={'_id':0}&" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      res.send(body);
    }
  });
});


// Obtener cuentaporidCuenta
app.get(URI + 'Cuenta', function(req, res) {
  var idCuenta = req.headers.idcuenta;
  var query = 'q={"idCuenta":' + idCuenta + '}';

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + query + "&l=1&" + apiKey);

  console.log(urlMlabRaiz + "/Cuentas?f={'_id':0}&" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      res.send(body);
    }
  });
});

// Obtener Cuentas
app.get(URI +"Cuentas", function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
          res.send(body);
        };
    });
});

//Acreditar saldo
app.put(URI + 'Cuenta/:idCuenta', function(req, res){

    let id = req.params.idCuenta;
    var queryFiltro = 'q={"idCuenta":' + id + '}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + queryFiltro + "&l=1&" + apiKey);
    clienteMlab.get('', function(error, resM, body){
        console.log(body);

      if (!error) {
        if (body.length > 0){

          var saldoActual = body[0].saldo
          var cuenta = body[0];
          cuenta.saldo = saldoActual + req.body.Importe;

          if(saldoActual > 0)
          {
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/account");

            clienteMlab.put('?q={"idCuenta": ' + body[0].idCuenta + '}&' + apiKey, cuenta ,
               function(errP, resP, bodyP) {
                   if (!errP) {
                     res.send("Importe acreditado correctamente");
                   }else{
                     res.status(500).send('Error al acreditar el importe');
                   }
               })
            }
            else{
              res.send('La cuenta no tiene saldo');
            }
        }
        else {
          res.status(404).send('Cuenta no encontrada');
        }
      }
      else{
        res.status(500).send('Error obteniendo la cuenta');
      }
    });
});

//Acreditar saldo
app.put(URI + 'Cuentaprueba/:id', function(req, res){

    let id = req.params.id; //Usuario
    var queryFiltroCuenta = 'q={"idUsuario":' + id + '}';
    var queryFiltroUsuario = 'q={"id":' + id + '}';

    //Obtengo el usuario
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?f={'_id':0}&" + queryFiltroUsuario + "&l=1&" + apiKey);

    clienteMlab.get('', function(error, resM, body){

      if (!error) {
        if (body.length > 0){
          if(body[0].logged == true)
          {
            queryFiltroCuenta = 'q={"idUsuario": 66}, {"idCuenta": 1}'

              //Obtengo las cuentas del usuario logueado
              clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?f={'_id':0}&" + queryFiltroCuenta + "&l=1&" + apiKey);
              clienteMlab.get('', function(errorCuenta, resM, bodyCuenta){
                  console.log(bodyCuenta);

                if (!errorCuenta) {
                  if (bodyCuenta.length > 0){

//bodyCuenta.idCuenta
                    var saldoActual = bodyCuenta[0].saldo
                    var cuenta = bodyCuenta[0];
                    cuenta.saldo = saldoActual + req.body.Importe;
console.log(cuenta);
                    if(saldoActual > 0)
                    {
                      clienteMlab = requestJson.createClient(urlMlabRaiz + "/account");

                      clienteMlab.put('?q={"idCuenta": ' + bodyCuenta[0].idCuenta + '}&' + apiKey, cuenta ,
                         function(errP, resP, bodyP) {
                             if (!errP) {
                               res.send("Importe acreditado correctamente");
                             }else{
                               res.status(500).send('Error al acreditar el importe');
                             }
                         })
                      }
                      else{
                        res.send('La cuenta no tiene saldo');
                      }
                  }
                  else {
                    res.status(404).send('Cuenta no encontrada');
                  }
                }
                else{
                  res.status(500).send('Error obteniendo la cuenta');
                }
              });
            }
            else{
              res.status(200).send('Algunos de los datos no son correctos');
            }
        }
        else {
          res.status(404).send('Usuario no encontrado');
        }
      }
      else{
        res.status(500).send('Error obteniendo usuario');
      }
    });


});


// Obtener Movimientos pendiente
app.get(URI + 'Movimientos', function(req, res) {
  var idcuenta = req.headers.idcuenta;
  var query = 'q={"iban":"' + idcuenta + '"}';
  var filter = 'f={"movimientos":1,"_id":0}';
  console.log(query);
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/account?" + query + "&" + filter + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      res.send(body);
    }
  })
})
